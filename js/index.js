$(function(){
  const swiper = new Swiper('.swiper', {
      loop: true,
      slidesPerView: 1,
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      }
    });

    $('.back-top a').click(function(e){
      e.preventDefault();
      $('body,html').animate({scrollTop: 0});
  });


  $('#hfcc2023 .menu a').click(function(e){
    e.preventDefault();
    let element = $(this).attr('href');
    let wrapHeaderHeight = $('#hfcc2023 .header').height();
    let contentHeader = $('#header').height();
    let elemTop = $(element).offset().top;
    let lastScrollTop = $(window).scrollTop();

    if(element === '#hfcc2023-recommend'){
      elemTop-=40;
    }

    let lastTop;
    if(lastScrollTop < elemTop){
      console.log('down');
      lastTop = Math.floor(elemTop - wrapHeaderHeight);
    }else{
      console.log('up');
      lastTop = Math.floor(elemTop - (wrapHeaderHeight + contentHeader));
    }

    $('body,html').animate({scrollTop: lastTop})
    
  });


  $(window).scroll(function(e){
    let scrollTop = $(window).scrollTop();
    let header = $('#header');
    let page = $('#page');
    let lastSection = $('#hfcc2023 .se9').offset().top;

    if(scrollTop >= lastSection){
      $('.fixed-bottom-menu').addClass('hide');
    }else{
      $('.fixed-bottom-menu').removeClass('hide');
    }

    if(scrollTop >= page.offset().top){
      if(header.is('[style="top: 0px;"]')){
        fixedHeaderHandler('top');
      }else if(header.is('[style]')){
        fixedHeaderHandler('space');
      }
    }else if(scrollTop < page.offset().top){
      fixedHeaderHandler();
    }
    
  });

});


function fixedHeaderHandler(value){
  switch(value){
    case 'top':
      $('#hfcc2023 .header').removeClass('fixed-top').addClass('fixed-space');
      break;
    case 'space':
      $('#hfcc2023 .header').removeClass('fixed-space').addClass('fixed-top');
      break;
    default:
      $('#hfcc2023 .header').removeClass('fixed-space fixed-top');
      break;
  }
}