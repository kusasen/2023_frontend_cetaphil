
$(function(){
  

  $('.mob-btn-menu').click(function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    if($(this).hasClass('active')){
      $('.navigation-box').show();
    }else{
      $('.navigation-box').hide();
    }
  });
  
  $('.item.first-menu').click(function(e){
    e.stopPropagation();
    let parent = $(this).parent('li');
    parent.toggleClass('active'); // li add Class
    parent.siblings().removeClass('active');

    if(parent.hasClass('active')){
      parent.siblings().find('.first-menu.active').removeClass('active');
      $(this).addClass('active');
    }else{
      $('.menu li.acitve').removeClass('active');
      $('.first-menu.active').removeClass('active');
      $(this).removeClass('active');
    }
  });

  $('.item.second-menu').click(function(e){
    e.stopPropagation();
    let parent = $(this).closest('.first-menu');
    parent.find('.second-menu').removeClass('active');
    $(this).addClass('active');
  });

  $(".anchorpoint a").click(function(e){
    e.preventDefault();
    let element = $(this).attr('href');
    $('body,html').animate({scrollTop: $(element).offset().top},500);

  });

  let _hash = window.location.hash;

  if(_hash){
    $('body,html').animate({scrollTop: $(_hash).offset().top});
  }


  $(window ).scroll(function() {
    $('#fixed-btns').fadeOut();
    clearTimeout( $.data( this, "scrollCheck" ) );
    $.data( this, "scrollCheck", setTimeout(function() {
      $('#fixed-btns').fadeIn();
    }, 800) );
});

});